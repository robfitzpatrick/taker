module gitlab.com/robfitzpatrick/cam

go 1.14

require (
	github.com/carbocation/interpose v0.0.0-20161206215253-723534742ba3
	github.com/go-bootstrap/go-bootstrap v0.0.0-20200531174834-c980814f3a58 // indirect
	github.com/golang/protobuf v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/sessions v1.2.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0 // indirect
	github.com/miekg/pkcs11 v1.0.3
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.1
	github.com/tylerb/graceful v1.2.15
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc
	google.golang.org/grpc v1.31.0
	google.golang.org/grpc/examples v0.0.0-20200814200710-a3740e5ed326
)
