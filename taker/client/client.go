package main

import (
	"context"
	"flag"
	"fmt"
	pb "gitlab.com/robfitzpatrick/cam/taker/taker_pb"
	"google.golang.org/grpc"
	"log"
	"os"
	"time"
)

var (
	tls                = flag.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
	caFile             = flag.String("ca_file", "", "The file containing the CA root cert file")
	serverAddr         = flag.String("server_addr", "localhost:10000", "The server address in the format of host:port")
	serverHostOverride = flag.String("server_host_override", "x.test.youtube.com", "The server name used to verify the hostname returned by the TLS handshake")
)

func main() {
	flag.Parse()
	var opts []grpc.DialOption

	if *tls {
		/*
		if *caFile == "" {
			*caFile = data.Path("x509/ca_cert.pem")
		}
		creds, err := credentials.NewClientTLSFromFile(*caFile, *serverHostOverride)
		if err != nil {
			log.Fatalf("Failed to create TLS credentials %v", err)
		}
		opts = append(opts, grpc.WithTransportCredentials(creds))

		 */
	} else {
		opts = append(opts, grpc.WithInsecure())
	}

	opts = append(opts, grpc.WithBlock())
	conn, err := grpc.Dial(*serverAddr, opts...)
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	}
	defer conn.Close()
	client := pb.NewTakerClient(conn)

	// Looking for a valid feature

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	photoBytes, err := client.TakeSinglePhoto(ctx, &pb.EmptyMessage{})
	if err != nil {
		log.Fatalf("%v.GetFeatures(_) = _, %v: ", client, err)
	}


	fileName := fmt.Sprintf("/tmp/photo_%v.jpg", time.Now().String())
    f, fileCreateErr := os.Create(fileName)
    if nil != fileCreateErr {
    	log.Fatalf("Failed to create file %v\n", fileName)
	}

	defer f.Close()
	_, fileWriteErr := f.Write(photoBytes.GetPhotoJpeg())
	if nil != fileWriteErr {
		log.Fatalf("Failed to write to %v\n", fileName)
	}

    log.Printf("Saved as %v\n", fileName)



}
