package main

import (
	"context"
	"flag"
	"fmt"
	pb "gitlab.com/robfitzpatrick/cam/taker/taker_pb"
	"google.golang.org/grpc"
	"log"
	"net"
	"time"
)

var (
	tls      = flag.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
	certFile = flag.String("cert_file", "", "The TLS cert file")
	keyFile  = flag.String("key_file", "", "The TLS key file")
	port     = flag.Int("port", 8888, "The server port")
)

func main() {

	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption

	if *tls {
		/*
			if *certFile == "" {
				*certFile = data.Path("x509/server_cert.pem")
			}
			if *keyFile == "" {
				*keyFile = data.Path("x509/server_key.pem")
			}
			creds, err := credentials.NewServerTLSFromFile(*certFile, *keyFile)
			if err != nil {
				log.Fatalf("Failed to generate credentials %v", err)
			}
			opts = []grpc.ServerOption{grpc.Creds(creds)}

		*/
	}

	grpcServer := grpc.NewServer(opts...)
	pb.RegisterTakerServer(grpcServer, newServer())
	grpcServer.Serve(lis)
}

type TakerServer struct {
}

var conn net.Conn

func (this *TakerServer) TakeSinglePhoto(context.Context, *pb.EmptyMessage) (newPhoto *pb.Photo, err error) {

	log.Printf("Handling request for a single photo\n")
	var connErr error
	if conn == nil {
		conn, connErr = net.Dial("tcp", "127.0.0.1:10000")
		if nil != connErr {
			err = connErr
			log.Fatalf("connErr was non-nil: %v\n", connErr.Error())
			return
		}
		log.Printf("Established connection to camera daemon\n")
	}

	_, connWriteErr := conn.Write([]byte{'s', 'n', 'a', 'p'})
	if nil != connWriteErr {
		err = connWriteErr
		return
	}
	log.Printf("Wrote photo request\n")

	readBuf := make([]byte, 1024*1024*12)

	numReadTotal := 0
	conn.SetReadDeadline(time.Now().Add(time.Millisecond * 5000))
	numRead, connReadErr := conn.Read(readBuf[numReadTotal:])
	numReadTotal += numRead
	log.Printf("Read %v bytes\n", numRead)
	if nil != connReadErr {
		log.Printf("connReadErr: %v\n", connReadErr.Error())
		return
	}

	for numRead != 0 {
		conn.SetReadDeadline(time.Now().Add(time.Millisecond * 500))
		var readErr error
		numRead, readErr = conn.Read(readBuf[numReadTotal:])
		if readErr != nil {
			break
		}
		numReadTotal += numRead
		log.Printf("Read %v bytes\n", numRead)
	}

	if 0 != numReadTotal {
		newPhoto = &pb.Photo{PhotoJpeg: readBuf[:numReadTotal]}
	}

	return
}

func newServer() *TakerServer {
	s := &TakerServer{}
	return s
}
