#!/bin/sh
set -e

ssh pi@192.168.178.41 "sudo pkill -f \"server\""

env GOOS=linux GOARCH=arm GOARM=5 go build -o server_arm
scp server_arm pi@192.168.178.41:server
scp picam-daemon.py pi@192.168.178.41:
