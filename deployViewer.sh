#!/bin/sh

cd viewer
env GOOS=linux GOARCH=arm GOARM=5 go build -o viewer_arm
cd ..

ssh pi@192.168.178.42 "sudo pkill -f \"viewer_arm\""

scp viewer/viewer_arm pi@192.168.178.42:
scp -r viewer/static pi@192.168.178.42:
rm viewer/viewer_arm

ssh pi@192.168.178.42 "sudo fuser -k 8080/tcp
    pkill -f \"chromium-browser\""


ssh pi@192.168.178.42 "./viewer_arm&
    export DISPLAY=:0.0
    sleep 3
    chromium-browser --start-fullscreen http://localhost:8080"

