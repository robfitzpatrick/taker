package main

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"time"
	pb "gitlab.com/robfitzpatrick/cam/taker/taker_pb"
)


func main() {

	fileServer := http.FileServer(http.Dir("./static"))
	http.Handle("/", fileServer)

	http.HandleFunc("/image.jpg", getLatestImage)

	http.ListenAndServe(":8080", nil)
}


func getLatestImage(w http.ResponseWriter, request *http.Request) {

	log.Printf("Called getLatestImage\n")

	var opts []grpc.DialOption

	if false {
		/*
			if *caFile == "" {
				*caFile = data.Path("x509/ca_cert.pem")
			}

			// For mTLS
			credentials.NewTLS()

			creds, err := credentials.NewClientTLSFromFile(*caFile, *serverHostOverride)
			if err != nil {
				log.Fatalf("Failed to create TLS credentials %v", err)
			}
			opts = append(opts, grpc.WithTransportCredentials(creds))
		*/

	} else {
		opts = append(opts, grpc.WithInsecure())
	}

	opts = append(opts, grpc.WithBlock())
	conn, err := grpc.Dial("192.168.178.41:8888", opts...)
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	}
	defer conn.Close()
	client := pb.NewTakerClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	photoBytes, err := client.TakeSinglePhoto(ctx, &pb.EmptyMessage{})
	if err != nil {
		if nil == photoBytes {
			log.Printf("    nil photoBytes returned...\n")
		}
		log.Printf("    len photo bytes: %v\n", len(photoBytes.PhotoJpeg))
		log.Fatalf("err occurred.TakeSinglePhoto(_) = _: %v", err.Error())
	}

	w.Header().Set("Content-Type", "image/jpeg")
	log.Printf("Writing %v bytes\n", len(photoBytes.PhotoJpeg))
	w.Write(photoBytes.PhotoJpeg)

}